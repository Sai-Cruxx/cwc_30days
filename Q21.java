package cwc30;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Q21 {
	    static List<List<Integer>> numQ(int n) {
	       
	        cols = new boolean[n];
	        lDig = new boolean[2*n];
	        rDig = new boolean[2*n];
	        sum  = new ArrayList<>();
	        List<Integer> temp = new ArrayList<>();
	        for(int i=0 ; i<n ; i++)
	        	temp.add(0);
	        qSolve(sum , n , 0 , temp);
	         
	        return sum;
	    }
	    private static void qSolve(List<List<Integer>> result,int n,int row,List<Integer> combined){
	        if(row==n){
	            result.add(new ArrayList<>(combined));
	            return;
	        }
	        for(int col = 0;col<n;col++){
	            if(cols[col] || lDig[row+col] || rDig[row-col+n])
	                continue;
	            cols[col] = lDig[row+col] = rDig[row-col+n] = true;
	            combined.set(col,row+1);
	            qSolve(result,n,row+1,combined);
	            cols[col] = lDig[row+col] = rDig[row-col+n] = false;
	        }
	    }
	  static List<List<Integer> > sum = new ArrayList<List<Integer> >();
	   static boolean[] cols,lDig,rDig;

	    public static void main(String[] args)
	    {
	       Scanner sc = new Scanner(System.in);
	       int n = sc.nextInt();
	 
	        List<List<Integer> > res = numQ(n);
	        System.out.println(res);
	    }
}
