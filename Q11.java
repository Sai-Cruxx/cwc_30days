package cwc30;

import java.util.Arrays;
import java.util.Scanner;

public class Q11 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double[] arr = new double[n];
		while(n > 0) {
			System.out.println("Enter the coordinates of 1st point: ");
			int x1 = sc.nextInt();
			int y1 = sc.nextInt();
			System.out.println("Enter the coordinates of 2nd point: ");
			int x2 = sc.nextInt();
			int y2 = sc.nextInt();
			
			double diff = Math.pow(x1-x2, 2);
			double diff2 = Math.pow(y1-y2, 2);
			double res = diff + diff2;
			double res2 = Math.sqrt(res);
			System.out.println(res);
			arr[n-1] = res2;
			n--;
		}
		
		Arrays.sort(arr);
		System.out.println("So the shortest path is: " + arr[0]);
		}

	}


